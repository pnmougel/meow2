#!/usr/bin/env bash

NAME=meow

BUILD_DIR=.build_tmp/meow-2.0

rm -Rf ${BUILD_DIR}
mkdir -p ${BUILD_DIR}
cp setup.py ${BUILD_DIR}
cp MANIFEST.in ${BUILD_DIR}
cp -R meow ${BUILD_DIR}
cp -R build_resources/debian ${BUILD_DIR}
cd ${BUILD_DIR}

dpkg-buildpackage -us -uc

cd ../..
mkdir -p dist/deb
cp .build_tmp/*.deb dist/deb/
rm -Rf .build_tmp
