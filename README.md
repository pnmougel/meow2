

## Install

```
sudo apt install libcairo2-dev

sudo apt install libgirepository1.0-dev
sudo apt install python-gi python-gi-cairo python3-gi python3-gi-cairo gir1.2-gtk-3.0
```

## Build

### Requirements

```
sudo apt install python3.6 python3-pip npm
sudo pip3 install pipenv

sudo npm install -g npm
sudo npm install -g @angular/cli
```

Create a new python environment

```
pipenv shell
```

### Building angular app

Install npm dependencies

```
python setup.py init_angular
```

or 

```
cd front
npm init
```

Build angular application

```
python setup.py build_angular
```

or

```
cd front
ng build --prod=true --optimization=false --outputPath=../meow/www
```

`--optimization=false` is required to disable minification and pass the line size rule for debian packages

### Run server in development mode

```
export FLASK_APP=meow_server:get_server
export FLASK_ENV=development
flask run --port=20100
```

### Build debian package

Ensure that the angular application has been built before creating the deb file.

```
sudo apt-get install build-essential debhelper devscripts equivs dh-virtualenv

./scripts/build_deb.sh
```

`build_deb.sh` is a wrapper around `dpkg-buildpackage` command that mostly clean generated files and move the deb file into `dist` folder.

### Install python package

```
python3 setup.py install
```

### Build pypi package

```
python3 setup.py sdist
```

### Upload to pypi

```
twine upload dist/MeowApp-2.0.0.tar.gz
```

#### SECURITY WARNING 

Currently, installing meow using pip does not allows to run the root server.
pip may install the meow_root.py script file in a user writable directory, and thus introduce a severe security 
breach. (i.e., any user can edit this file to perform actions as root).

### Using GtkWebkit instead of CEF

Chromium embeded framework is currently used but requires many dependencies and generates hard to fix errors when creating the deb files

GtkFront.py uses Webkit2 to render the app.

### Tmp...

dh clean --with python-virtualenv --python /usr/bin/python3.6
dh build --with python-virtualenv --python /usr/bin/python3.6
dh binary --with python-virtualenv --python /usr/bin/python3.6
