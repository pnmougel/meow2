export interface DesktopEntry {
  id: string;
  name: string;
  file_path: string;
  icon_img: string;
  icon_name?: string;
  categories: string[];
  keywords: string[];
  comment?: string;
  hidden: boolean;
  exec: string;
  require_admin: boolean;
}
