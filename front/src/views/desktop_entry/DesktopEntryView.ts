import {ChangeDetectorRef, Component, NgZone, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClientService} from "../../services/HttpClientService";
import {DesktopEntry} from "../../models/DesktopEntry";
import {AcceptDroppableView} from "../../services/AcceptDroppableView";

interface StartRootServerResponse {
  token: string
}

@Component({
  selector: 'desktop-entry',
  templateUrl: './desktop-entry.html',
  styleUrls: ['./desktop-entry.scss']
})
export class DesktopEntryView extends AcceptDroppableView implements OnInit {
  entry: DesktopEntry;
  entryId: string;

  isRootServerAlive = false;
  isEditable = false;
  entryVisible: boolean;
  isCreate: boolean;
  isUrl: boolean;
  urlLink: string;
  isConfirmDelete = false;

  constructor(private httpClient: HttpClientService,
              private route: ActivatedRoute,
              private cdr: ChangeDetectorRef,
              zone: NgZone,
              router: Router) {
    super(router, zone);

    // @ts-ignore
    window.handle_file_select = (filePath) => {
      this.entry.exec = filePath;
      // Force reload display
      this.cdr.detectChanges();
    };
  }

  canEdit() {
    this.isEditable = !!this.entry && (!this.entry.require_admin || this.isRootServerAlive);
    return this.isEditable;
  }

  async ngOnInit() {
    this.isCreate = !this.route.snapshot.params.id;
    if (this.isCreate) {
      const queryParams = this.route.snapshot.queryParams as DesktopEntry;
      const entryType = this.route.snapshot.queryParams.entryType;
      console.log(entryType);
      this.entry = {
        id: queryParams.id,
        name: queryParams.name,
        file_path: queryParams.file_path,
        icon_img: queryParams.icon_img,
        icon_name: queryParams.icon_name,
        categories: queryParams.categories,
        keywords: queryParams.keywords,
        comment: queryParams.comment,
        hidden: queryParams.hidden,
        exec: queryParams.exec,
        require_admin: queryParams.require_admin,
      }
    } else {
      this.entryId = this.route.snapshot.params.id;
      this.entry = (await this.httpClient.get(`desktop_entries/${this.entryId}`, true)) as DesktopEntry;
      this.entryVisible = !this.entry.hidden;
    }

    if(!!this.entry.categories) {
      this.entry.categories = this.entry.categories.filter(c => !!c);
    }
    this.isUrl = !!this.entry.exec && this.entry.exec.startsWith('xdg-open "http')
    if(this.isUrl) {
      this.urlLink = this.entry.exec.replace('xdg-open "', '').slice(0, -1);
    }
  }

  async unlock() {
    if (this.isRootServerAlive) {
      this.httpClient.killRootServer()
        .catch(err => {})
        .finally(() => {
          this.isRootServerAlive = false;
          localStorage.removeItem('token');
        });
    } else {
      let res = await this.httpClient.get('start_root') as StartRootServerResponse;
      this.isRootServerAlive = !!res.token;
      if (!!res.token) {
        localStorage.setItem('token', res.token);
      }
    }
  }

  selectFile($event) {
    if ($event.which === 1) {
      let fileInputElement = document.createElement('input');
      fileInputElement.type = 'file';
      fileInputElement.click()
    }
  }


  runEntry() {
    this.httpClient.get(`run/${this.entryId}`, true)
  }

  async updateEntry() {
    this.entry.hidden = !this.entryVisible;
    this.httpClient.put(`desktop_entries`, this.entry, !this.entry.require_admin).catch(error => {
    })
  }

  async createEntry() {
    this.httpClient.post(`desktop_entries`, this.entry, true).then(res => {
      return this.router.navigate(['desktop-entry'], { queryParams: {}});
    }).catch(error => {
    });

  }

  async deleteEntry() {
    this.httpClient.delete(`desktop_entries/${this.entry.id}`).then(res => {
      return this.router.navigate(['desktop-entry'], { queryParams: {}});
    }).catch(error => {
    });

  }

  updateExecFromLink() {
    this.entry.exec = `xdg-open "${this.urlLink}"`
  }
}
