import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './main.html',
  styleUrls: ['./main.scss']
})
export class MainView {
  title = 'meow';

  menu = [{
    name: 'Applications',
    link: '/desktop-entries',
    icon: 'far fa-window'
  }, {
    name: 'Folders',
    link: '/folders',
    icon: 'fa fa-folder'
  }, {
    name: 'Settings',
    link: '/settings',
    icon: 'fa fa-cog'
  }, {
    name: 'Synchronize',
    link: '/sync',
    icon: 'fa fa-sync'
  }];

  constructor() {}
}
