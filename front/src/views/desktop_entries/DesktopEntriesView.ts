import {ChangeDetectorRef, Component, HostListener, NgZone, OnInit} from "@angular/core";
import {HttpClientService} from "../../services/HttpClientService";
import {DesktopEntry} from "../../models/DesktopEntry";
import {Subject} from "rxjs";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";
import {ActivatedRoute, Router} from "@angular/router";
import {AcceptDroppableView} from "../../services/AcceptDroppableView";

@Component({
  selector: 'desktop-entry',
  templateUrl: './desktop_entries.html',
  styleUrls: ['./desktop_entries.scss']
})
export class DesktopEntriesView extends AcceptDroppableView implements OnInit {
  desktopEntries: DesktopEntry[];
  appFolders: any[];

  nameFilter = '';
  newFolderName = '';
  showDeleteConfirm = false;

  groupNameInput : Subject<[string, object]> = new Subject();

  folderView = false;
  showHidden = false;
  endpoint = 'desktop_entries';

  constructor(private httpClient: HttpClientService,
              private cdr: ChangeDetectorRef,
              private route: ActivatedRoute,
              zone: NgZone,
              router: Router) {
    super(router, zone);
    this.folderView = route.snapshot.data.folderView;
    this.endpoint = route.snapshot.data.endpoint;
  }

  async ngOnInit() {
    await this.updateEntries();

    // @ts-ignore
    if(!!window.register_drop_cb) {
      // @ts-ignore
      window.register_drop_cb((data) => {
        console.log(`FROM JS : ${data}`)
        console.log(`FROM JS : ${JSON.stringify(data, null, 2)}`)
      })
    }

    this.groupNameInput.pipe(debounceTime(500),distinctUntilChanged()).subscribe(async e =>{
      let [newName, appFolder] = e
      // @ts-ignore
      await this.httpClient.post(`app_folders/${appFolder.id}/rename/${newName}`, {}, true);
      await this.updateEntries();
    });
  }

  async addFolder(popover) {
    await this.httpClient.post(`app_folders/${this.newFolderName}`, {}, true);
    await this.updateEntries();

    this.newFolderName = '';
    popover.close()
  }

  async removeFolder(evt, folder) {
    await this.httpClient.delete(`app_folders/${folder.id}`, true);
    await this.updateEntries();
  }

  async updateGroupName(newName, appFolder) {
    this.groupNameInput.next([newName, appFolder])
  }

  async updateEntries() {
    this.appFolders = (await this.httpClient.get(this.endpoint, true)) as any[];
    this.appFolders.sort((a, b) => {
      if(!a.id) {
        return 1
      } else if(!b.id) {
        return -1
      }
      return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
    });
  }

  isEntryVisible(entry) {
    const nameFilter = this.nameFilter.toLowerCase();
    let isVisible = this.showHidden || entry.is_visible;
    if(!!nameFilter) {
      const searchTermLower = this.nameFilter.toLowerCase();
      let searchable = entry.name;
      if(!!entry.keywords) {
        searchable += ' ' + entry.keywords.join(" ")
      }
      const isMatch = !!searchable.toLowerCase().split(' ').find(e => e.startsWith(searchTermLower));
      isVisible = isVisible && isMatch
    }
    return isVisible;
  }

  selectFile(evt) {
    // @ts-ignore
    window.select_file((value) => {
      this.desktopEntries[0].name = value;

      // Change occurred outside of angular environment. We must force change detection
      this.cdr.detectChanges();
    });
  }

  /**
   * Disable default drag behavior
   */
  onDragStart(evt) {
    evt.stopPropagation();
    return false;
  }


  dragInfo = null;
  isDragging = false;

  dragStart(event, entry, appFolder) {
    this.isDragging = true;
    this.dragInfo = {
      entry: entry,
      appFolder: appFolder,
      ctrlKey: event.ctrlKey
    };

    // Set drag image
    let dragImage = document.getElementById('drag-image') as HTMLImageElement;
    if(!!entry.icon_img) {
      dragImage.src = `http://localhost:20100/icon/${entry.icon_img}`;
    } else {
      dragImage.src = 'assets/missing.png'
    }
  }

  @HostListener('document:mousemove', ['$event'])
  docMouseMove(event) {
    if(this.isDragging) {
      this.dragInfo.ctrlKey = event.ctrlKey;
      let ball = document.getElementById('scroll_area');
      ball.style.display = 'block';
      ball.style.left = event.pageX - 50 + 'px';
      ball.style.top = event.pageY - 50 + 'px';
    }
  }


  @HostListener('document:mouseup', ['$event'])
  docMouseUp(event) {
    if(this.isDragging) {
      this.dragInfo.ctrlKey = event.ctrlKey;
      this.isDragging = false;
      let ball = document.getElementById('scroll_area')
      ball.style.display = 'none';
      let elemBelow = document.elementFromPoint(event.clientX, event.clientY);
      if(!!elemBelow) {
        let droppableBelow = elemBelow.closest('.droppable');
        if(!!droppableBelow) {
          // Send a fake DragEvent to trigger Drop
          droppableBelow.dispatchEvent(new DragEvent("drop"))
        }
      }
    }
  }

  @HostListener('document:wheel', ['$event'])
  docWheel(event) {
    if(this.isDragging) {
      document.getElementById('view-content').scrollBy(0, event.deltaY);
    }
  }

  async openNewFolder(folderPopover) {
    folderPopover.open();
    const input: HTMLInputElement = document.getElementById('newFolderName') as HTMLInputElement;
    input.focus();
  }

  async onDrop(evt, entryGroup) {
    let isCopy = this.dragInfo.ctrlKey;
    const desktop_entry_id = this.dragInfo.entry.id;
    const entry_group_id = this.dragInfo.appFolder.id;

    if(!!desktop_entry_id && entry_group_id !== entryGroup.id) {
      if(!!entryGroup.id) {
        // Add entry to group
        await this.httpClient.post(`app_folders/${entryGroup.id}/add_entry/${desktop_entry_id}`, {}), true;
      }
      if(!isCopy && !!entry_group_id) {
        await this.httpClient.post(`app_folders/${entry_group_id}/remove_entry/${desktop_entry_id}`, {}, true);
      }
      await this.updateEntries();
    }
  }

  addEntry(entryType) {
    this.router.navigate(['desktop-entry', 'create'], { queryParams: {entryType}});
  }

}
