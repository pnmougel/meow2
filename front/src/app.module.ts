import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';

import {NgxPopperModule} from 'ngx-popper';
import {DesktopEntryView} from "./views/desktop_entry/DesktopEntryView";
import {DesktopEntriesView} from "./views/desktop_entries/DesktopEntriesView";
import {MainView} from "./views/main/MainView";
import {RouterModule, Routes} from "@angular/router";
import {NgSelectModule} from '@ng-select/ng-select';
import {AutosizeModule} from 'ngx-autosize';

const routes: Routes = [
  {path: 'desktop-entry/create', component: DesktopEntryView},
  {path: 'desktop-entry/:id', component: DesktopEntryView},
  {
    path: 'desktop-entries/', component: DesktopEntriesView, data: {
      folderView: false,
      endpoint: 'desktop_entries'
    },
  },
  {
    path: 'folders', component: DesktopEntriesView, data: {
      folderView: true,
      endpoint: 'app_folders'
    }
  },
  {path: '**', component: DesktopEntriesView, data: {
      folderView: false,
      endpoint: 'desktop_entries'
    }},
  {path: '', component: DesktopEntriesView, data: {
      folderView: false,
      endpoint: 'desktop_entries'
    }},
];


@NgModule({
  declarations: [
    MainView,
    DesktopEntryView,
    DesktopEntriesView,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    NgxPopperModule,
    FormsModule,
    NgSelectModule,
    AutosizeModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [MainView]
})
export class AppModule {
}
