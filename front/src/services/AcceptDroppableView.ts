import {Router} from "@angular/router";
import {NgZone} from "@angular/core";

export class AcceptDroppableView {
  constructor(protected router: Router, protected zone: NgZone) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;

    // @ts-ignore
    window.handle_drop = (appInfo) => {
      this.zone.run(async () => {
        // await this.router.navigateByUrl('/desktop-entry/create/' + appInfo.name, { queryParams: appInfo})
        await this.router.navigate(['desktop-entry', 'create'], { queryParams: appInfo, replaceUrl: true});
      });
    };
  }
}
