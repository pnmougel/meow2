import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

interface RootServerAliveResponse {
  is_root_server_alive: boolean
}

@Injectable({
  providedIn: 'root',
})
export class HttpClientService {
  notRootServer = 'http://localhost:20100';
  rootServer = 'http://localhost:20101';

  constructor(private http: HttpClient) {}

  getHeaders() {
    let headers = new HttpHeaders();
    let token = localStorage.getItem('token');
    if(!!token) {
      headers = headers.append('Authorization', token)
    }
    return headers;
  }

  async isRootAlive() {
    const res = await this.http.get<RootServerAliveResponse>(`${this.notRootServer}/is_root_up`).toPromise();
    return res.is_root_server_alive;
  }

  async get(path: string, forceNotRoot: boolean = false) {
    return this.http.get(`${await this.getServer(forceNotRoot)}/${path}`, {headers: this.getHeaders()}).toPromise()
  }

  async post(path: string, body: any, forceNotRoot: boolean = false) {
    return this.http.post(`${await this.getServer(forceNotRoot)}/${path}`, body, {headers: this.getHeaders()}).toPromise()
  }

  async put(path: string, body: any, forceNotRoot: boolean = false) {
    return this.http.put(`${await this.getServer(forceNotRoot)}/${path}`, body, {headers: this.getHeaders()}).toPromise()
  }

  async delete(path: string, forceNotRoot: boolean = false) {
    return this.http.delete(`${await this.getServer(forceNotRoot)}/${path}`, {headers: this.getHeaders()}).toPromise()
  }

  async killRootServer() {
    return this.http.get(`${this.rootServer}/kill`, {headers: this.getHeaders()}).toPromise()
  }

  async getServer(forceNotRoot: boolean) {
    if(forceNotRoot) {
      return this.notRootServer;
    }
    const isRootAlive = await this.isRootAlive();
    return isRootAlive ? this.rootServer : this.notRootServer;
  }

}
