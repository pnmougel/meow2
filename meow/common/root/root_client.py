import os
import secrets
import subprocess
import sys
from time import sleep

import requests

# Use if DEV because we only want to be able to use script if in dev mode
# Otherwise it could be possible to write a custom meow_server.py script and put it
# where the user run the meow command
from urllib3.exceptions import NewConnectionError

MEOW_ROOT_BIN_PATH = '/usr/bin/meow_root'
MEOW_ROOT_SCRIPT = 'meow_root.py'


class RootClient:
    def __init__(self, is_dev: bool):
        self.is_dev = is_dev
        self.conn = None
        self.proc = None
        self.key = None

    @staticmethod
    def is_root_server_up():
        try:
            requests.get('http://localhost:20101/is_alive', timeout=1)
            return True
        except requests.exceptions.ConnectionError as e:
            return False

    def get_pkexec_command(self):
        if self.is_dev:
            meow_root_script_path = os.path.join(os.getcwd(), MEOW_ROOT_SCRIPT)
            print(meow_root_script_path)
            if os.path.isfile(meow_root_script_path):
                # return [sys.executable, meow_root_script_path]
                return ['pkexec', sys.executable, meow_root_script_path]
        else:
            if os.path.isfile(MEOW_ROOT_BIN_PATH):
                return ['pkexec', MEOW_ROOT_BIN_PATH]
        return None

    def start_root_server(self):
        if RootClient.is_root_server_up():
            return None

        pkexec_command = self.get_pkexec_command()

        if pkexec_command is not None:
            key = secrets.token_hex(32)
            self.proc = subprocess.Popen(pkexec_command, stdin=subprocess.PIPE)
            self.proc.stdin.write(key.encode('utf-8'))
            self.proc.stdin.write(b'\n')
            self.proc.stdin.flush()

            retry = 0
            while retry < 20 and not RootClient.is_root_server_up():
                sleep(0.5)
                retry += 1

            if RootClient.is_root_server_up():
                return key
            else:
                return None


root_client = RootClient(True)
