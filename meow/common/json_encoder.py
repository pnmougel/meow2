from flask.json import JSONEncoder


import logging
logger = logging.getLogger(__name__)


class CustomJSONEncoder(JSONEncoder):
    json_convert_method = 'to_json'

    def default(self, obj):
        try:
            if hasattr(obj, CustomJSONEncoder.json_convert_method):
                to_json_call = getattr(obj, CustomJSONEncoder.json_convert_method, None)
                if callable(to_json_call):
                    return to_json_call()
            iterable = iter(obj)
        except TypeError as e:
            logger.error('Unable to serialize to JSON', e)
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)
