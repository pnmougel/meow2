import logging
import logging.config

import coloredlogs

log_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {
            "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        }
    },

    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "simple",
            "stream": "ext://sys.stdout"
        },

        "info_file_handler": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": "INFO",
            "formatter": "simple",
            "filename": "logs/info/info.log",
            "maxBytes": 10485760,
            "backupCount": 20,
            "encoding": "utf8"
        },

        "error_file_handler": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": "ERROR",
            "formatter": "simple",
            "filename": "logs/error/errors.log",
            "maxBytes": 10485760,
            "backupCount": 20,
            "encoding": "utf8"
        }
    },

    "loggers": {
        "my_module": {
            "level": "ERROR",
            "handlers": ["console"],
            "propagate": False
        }
    },

    "root": {
        "level": "INFO",
        "handlers": ["info_file_handler", "error_file_handler"]
    }
}


def init_logs(is_prod: bool):
    if is_prod:
        # Disable file logging in prod environment
        log_config['root']['handlers'] = []
        del log_config['handlers']['info_file_handler']
        del log_config['handlers']['error_file_handler']

    logging.config.dictConfig(log_config)
    logging.getLogger("werkzeug").setLevel(logging.CRITICAL)

    coloredlogs.DEFAULT_DATE_FORMAT = '%H:%M:%S'
    coloredlogs.install(fmt='%(levelname)-8s %(name)-10s %(asctime)s - %(message)s')
