from urllib.parse import urlparse
from urllib.request import urlretrieve
from bs4 import BeautifulSoup
from pathlib import Path
import os

import gi
from gi.repository.Gtk import IconInfo
from unidecode import unidecode
import re
import requests

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

icon_theme = Gtk.IconTheme.get_default()


def get_app_info_from_url(url: str):
    parsed_uri = urlparse(url)
    base_url = f'{parsed_uri.scheme}://{parsed_uri.netloc}'
    main_domain_name = parsed_uri.netloc.split('.')[-2]

    r = requests.get(url, stream=True)
    soup = BeautifulSoup(r.text, 'html.parser')
    # Get name
    name = main_domain_name
    file_name = main_domain_name
    title_element = soup.find('title')
    if title_element is not None:
        name = main_domain_name.capitalize() + " - " + title_element.text
        clean_name = unidecode(name).lower()
        clean_name = re.sub(r'\W+', '', clean_name)
        file_name = f'{file_name}-{clean_name}'

    icon_info: IconInfo = icon_theme.lookup_icon(main_domain_name, 128, 0)
    if icon_info is None:
        # print(page.read())
        soup = BeautifulSoup(r.text, 'html.parser')
        res = soup.find_all("link", attrs={
            "rel": ['image_src', 'apple-touch-icon', 'icon', 'fluid-icon']
        })
        best_icon_url = base_url + '/favicon.ico'
        best_icon_score = 0
        for elem in res:
            if elem.has_attr('href'):
                size = 0
                if elem.has_attr('sizes') and 'x' in elem.attrs["sizes"]:
                    try:
                        size = int(elem.attrs["sizes"].split('x')[0])
                    except ValueError:
                        pass

                # Compute icon score
                score = size if size >= 60 else 5
                if 'fluid-icon' in elem['rel']:
                    score = max(score, 30)
                elif "apple-touch-icon" in elem['rel']:
                    score = max(score, 20)
                elif "shortcut" in elem['rel']:
                    score = max(score, 10)

                print(score)

                if score > best_icon_score:
                    best_icon_score = score
                    href = elem['href']
                    if not href.startswith("http"):
                        if href.startswith("//"):
                            href = "http:" + href
                        else:
                            if not href.startswith("/"):
                                href = "/" + href
                            href = base_url + href
                    best_icon_url = href
        parsed_uri = urlparse(best_icon_url)

        # Save icon
        filename, file_extension = os.path.splitext(parsed_uri.path)
        base_icon_path = f'{Path.home()}/.local/share/icons'
        if not os.path.exists(base_icon_path):
            os.makedirs(base_icon_path)
        icon_path = f'{base_icon_path}/{main_domain_name}{file_extension}'
        urlretrieve(best_icon_url, icon_path)

    return {
        'name': name,
        'path': f'{Path.home()}/.local/share/applications/{file_name}.desktop',
        'file_name': file_name,
        'keywords': [main_domain_name],
        'icon_name': main_domain_name,
        'exec': f'xdg-open "{url}"',
        'categories': ['Website', main_domain_name]
    }


# soup = BeautifulSoup("<link rel='image_src'", 'html.parser')
# res = soup.find_all("link", attrs={
#     "rel": ['image_src', 'apple-touch-icon', 'icon', 'fluid-icon']
# })
# print(res)

# url = 'https://help.launchpad.net/Packaging/PPA/Uploading'


# def get_from_header(url):
#     doc = b''
#     r = requests.get(url, stream=True)
#     for line in r.iter_lines():
#         line_lower = line
#         doc += line
#         if b'</head>' in line_lower.lower():
#             break
#     return doc
    # soup = BeautifulSoup(doc, 'html.parser')
    # return soup.find_all("link", attrs={
    #     "rel": ['image_src', 'apple-touch-icon', 'icon', 'fluid-icon']
    # })


# def get_all(url):
#     r = requests.get(url)
#     return r.text
    # soup = BeautifulSoup(r.text, 'html.parser')
    # return soup.find_all("link", attrs={
    #     "rel": ['image_src', 'apple-touch-icon', 'icon', 'fluid-icon']
    # })

# import time


# start_time = time.time()
# d1 = 0
# d2 = 0
# for i in range(0, 20):
#     start_time = time.time()
#     get_from_header(url)
#     d1 += time.time() - start_time
#
#     start_time = time.time()
#     get_all(url)
#     d2 += time.time() - start_time
#
# print(d1)
# print(d2)

# app_info = get_app_info_from_url('https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-index_.html')
# print(app_info)
# DesktopEntryRepository.create_desktop_entry(
#     app_info['icon'], app_info['name'], app_info['keywords'], app_info['icon'], app_info['command'], app_info['categories']
# )
# print(get_app_info_from_url('http://stackoverflow.com/questions/1234567/blah-blah-blah-blah'))
# print(get_app_info_from_url('http://www.favicomatic.com/'))
