#!/usr/bin/env python3.6

import os
import sys

from flask import Flask
from flask_cors import CORS
from flask_json import FlaskJSON
from gevent.pywsgi import WSGIServer

cur_dir = os.path.dirname(__file__)
if cur_dir not in sys.path:
    sys.path.append(os.path.dirname(__file__))

from common.json_encoder import CustomJSONEncoder
from common.logs import init_logs

from domains.app_folders.handler import app_folders_handler
from domains.desktop_entries.handler import desktop_entries_handler, desktop_entry_update_handler
from domains.icons.handler import icons_handler
from domains.misc.handler import kill_handler, sudo_switch_handler, file_server_handler
from domains.app_folders.repository import folder_repo
from domains.desktop_entries.repository import de_repo

import logging

SERVER_PORT = 20100
logger = logging.getLogger(__name__)


def get_server(is_prod=False):
    init_logs(is_prod)

    if is_prod:
        logger.info("Start server using PROD environment")
    else:
        logger.info("Start server using DEV environment")

    de_repo.update_entries()
    folder_repo.update_folders()

    app = Flask(__name__)
    FlaskJSON(app)
    CORS(app)
    app.json_encoder = CustomJSONEncoder

    app.register_blueprint(desktop_entries_handler)
    app.register_blueprint(app_folders_handler)
    app.register_blueprint(icons_handler)
    app.register_blueprint(kill_handler)
    app.register_blueprint(sudo_switch_handler)
    app.register_blueprint(file_server_handler)
    app.register_blueprint(desktop_entry_update_handler)

    app.config.update({"JSON_ADD_STATUS": False})

    return app


def start_prod_server():
    # Set environment to production
    os.environ["FLASK_ENV"] = "production"
    app = get_server(True)

    http_server = WSGIServer(('', SERVER_PORT), app)
    try:
        http_server.serve_forever()
    except Exception as e:
        logger.error("Unable to start server", e)
