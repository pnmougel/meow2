import sys
import gi

from domains.desktop_entries.repository import DesktopEntryRepository
from favicon import get_app_info_from_url

gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, WebKit2
import json

# Info :
# A very hackish way to run python code from js is using signals.
# E.g., in js, try to create a notification with some content
# get 'notification' signal in python code and do stuff depending on the content of the notification




class Browser(Gtk.Window):
    def __init__(self, *args, **kwargs):
        super(Browser, self).__init__(*args, **kwargs)

        self.connect("destroy", Gtk.main_quit)
        self.set_default_size(1280, 800)

        self.webview = WebKit2.WebView()
        self.webview.load_uri("http://localhost:4200/index.html")
        # self.webview.load_uri("http://localhost:20100/index.html")

        self.webview.get_settings().set_enable_write_console_messages_to_stdout(True)

        self.webview.get_settings().set_property("enable_write_console_messages_to_stdout", True)
        self.webview.get_settings().set_property('enable-developer-extras', True)
        self.webview.get_settings().set_property('allow_file_access_from_file_urls', True)
        self.webview.get_settings().set_property('allow_universal_access_from_file_urls', True)
        # self.webview.get_settings().set_property('enable_smooth_scrolling', True)
        print(self.webview.get_settings().get_allow_file_access_from_file_urls())
        print(self.webview.get_settings().get_allow_universal_access_from_file_urls())
        print(self.webview.get_settings().get_enable_write_console_messages_to_stdout())
        print(self.webview.get_settings().get_hardware_acceleration_policy())
        print(self.webview.get_settings().get_enable_smooth_scrolling())

        def disable_context_menu(web_view, context_menu, event, hit_test_result):
            return True
        self.webview.connect("context-menu", disable_context_menu)

        def on_drag_data_received(widget, context, x, y, time):
            print('Data received')
            print(context)
            return False

        self.webview.connect("drag_drop", on_drag_data_received)

        def get_file_chooser(web_view, request):
            request.cancel()
            print("Show file chooser")
            print(request.get_mime_types())
            # web_view.do_run_file_chooser(request)
            dialog = Gtk.FileChooserDialog("Please choose a file", self,
                                           Gtk.FileChooserAction.OPEN,
                                           (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                            Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
            dialog.set_keep_above(True)
            # dialog.set_filename(base_path)
            response = dialog.run()
            if response == Gtk.ResponseType.OK:
                js_cmd = "if(!!window.handle_file_select) { window.handle_file_select('" + dialog.get_filename() + "') }"
                self.webview.run_javascript(js_cmd, None)
                print(dialog.get_filename())
            dialog.destroy()
            return True
        self.webview.connect("run_file_chooser", get_file_chooser)

        def handle_load_changed(web_view, resource, request):
            uri = request.get_uri()
            # print(request.get_uri())
            is_valid_uri = uri.startswith("http://localhost")
            if not is_valid_uri:
                self.webview.stop_loading()
                if uri.startswith("http"):
                    app_info = get_app_info_from_url(uri)
                    app_info_json = json.dumps(app_info)
                    js_cmd = "if(!!window.handle_drop) { window.handle_drop(" + app_info_json + ") }"
                    self.webview.run_javascript(js_cmd, None)
                    # DesktopEntryRepository.create_desktop_entry(
                    #     app_info['icon'], app_info['name'], app_info['keywords'], app_info['icon'], app_info['command'], app_info['categories']
                    # )
                return False
            else:
                return True
        self.webview.connect("resource-load-started", handle_load_changed)

        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.add(self.webview)
        self.add(scrolled_window)
        scrolled_window.show_all()

        self.show()

        inspector = self.webview.get_inspector()
        inspector.attach()
        inspector.show()


if __name__ == "__main__":
    Gtk.init(sys.argv)
    browser = Browser()
    Gtk.main()
