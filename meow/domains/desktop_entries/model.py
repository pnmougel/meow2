import hashlib

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio
from gi.repository.Gio import DesktopAppInfo

import os
from domains.icons.repository import icon_repo

def as_list(content):
    if content is None:
        return []
    else:
        return content.split(';')


class DesktopEntryModel:
    def __init__(self, app_info: DesktopAppInfo):
        self.app_info = app_info

        self.id = hashlib.md5(app_info.get_filename().encode()).hexdigest()
        self.icon_path = None
        self.icon_img = None
        icon = app_info.get_icon()
        self.icon_path = icon_repo.get_icon_path(icon)
        if self.icon_path is not None:
            icon_extension = self.icon_path.split('.')[-1]
            self.icon_img = f'{self.id}.{icon_extension}'
        self.name = app_info.get_name()
        self.display_name = app_info.get_display_name()

        self.require_admin = not os.access(app_info.get_filename(), os.W_OK)
        self.path = app_info.get_filename()
        self.exec = app_info.get_executable()
        self.file_name = app_info.get_id()

        self.exec = app_info.get_commandline()
        self.categories = as_list(app_info.get_categories())
        self.keywords = app_info.get_keywords()
        # app_info.get_
        self.comment = app_info.get_description()

        self.is_visible = app_info.should_show()

    def run(self):
        self.app_info.launch()

    def to_dict_simple(self):
        return {
            "id": self.id,
            "require_admin": self.require_admin,
            "path": self.path,
            "name": self.name,
            "icon_img": self.icon_img,
            "is_visible": self.is_visible,
            'keywords': self.keywords
        }

    def to_json(self):
        res = vars(self).copy()
        del res['app_info']
        return res
