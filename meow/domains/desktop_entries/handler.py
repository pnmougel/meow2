import os

from flask import Blueprint, request
from flask_json import json_response

from domains.desktop_entries.model import DesktopEntryModel
from domains.desktop_entries.repository import de_repo as desktop_entry_repository
from unidecode import unidecode
import re

desktop_entries_handler = Blueprint('desktop_entries_handler', __name__)
desktop_entry_update_handler = Blueprint('desktop_entriy_update_handler', __name__)


@desktop_entries_handler.route('/desktop_entries', methods=['GET'])
def list_desktop_entries():
    desktop_entry_repository.update_entries()
    simple_entries = [de.to_dict_simple() for de in desktop_entry_repository.desktop_entries]

    def sort_by_name(e: DesktopEntryModel):
        return e['name'].lower()
    simple_entries.sort(key=sort_by_name)

    return json_response(data_=[{
        "entries": simple_entries,
        "name": "Applications"
    }])


@desktop_entries_handler.route('/desktop_entries/<entry_id>', methods=['GET'])
def get_desktop_entry(entry_id):
    return json_response(data_=desktop_entry_repository.get_entry_by_id(entry_id))


@desktop_entries_handler.route('/is_sudo')
def is_sudo():
    return json_response(data_={"is_sudo": os.geteuid() == 0})


@desktop_entries_handler.route('/run/<entry_id>', methods=['GET'])
def run_desktop_entry(entry_id):
    entry = desktop_entry_repository.get_entry_by_id(entry_id)
    if entry is not None:
        entry.run()
    return json_response(data_={
        'running': True
    })


@desktop_entries_handler.route('/desktop_entries/<entry_id>', methods=['DELETE'])
def delete_desktop_entry(entry_id):
    is_deleted = False

    entry = desktop_entry_repository.get_entry_by_id(entry_id)

    if entry is not None and entry.path is not None:
        os.remove(entry.path)
        is_deleted = True
    return json_response(data_={
        "deleted": is_deleted
    })


@desktop_entry_update_handler.route('/desktop_entries', methods=['PUT'])
def update_desktop_entry():
    body = request.get_json()
    desktop_entry_repository.update_desktop_entry(body)
    return json_response(data_={
        "updated": 'ok'
    })


@desktop_entry_update_handler.route('/desktop_entries', methods=['POST'])
def create_desktop_entry():
    body = request.get_json()

    if 'name' not in body:
        return json_response(data_={
            "error": 'missing required field name'
        })

    name = body['name']
    # Build file name
    file_name = unidecode(name).lower()
    file_name = re.sub(r'\s+', '_', file_name)
    file_name = re.sub(r'_+', '_', file_name)
    file_name = re.sub(r'\W+', '', file_name)

    desktop_entry_repository.create_desktop_entry(file_name, name,
                                                  body.get('keywords', []),
                                                  body.get('icon', ''),
                                                  body.get('exec', ''),
                                                  body.get('categories', []))
    return json_response(data_={
        "created": 'ok'
    })

