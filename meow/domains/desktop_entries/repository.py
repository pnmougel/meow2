from typing import Mapping, Iterable
import os
from pathlib import Path

import gi
from xdg import DesktopEntry

from domains.desktop_entries.model import DesktopEntryModel

gi.require_version('Gtk', '3.0')
from gi.repository import Gio


class DesktopEntryRepository:
    def __init__(self):
        self.desktop_entry_cache: Mapping[str, DesktopEntryModel] = {}
        self.category_to_desktop_entries: Mapping[str, Iterable[DesktopEntryModel]] = {}
        self.filename_to_desktop_entries: Mapping[str, DesktopEntryModel] = {}
        self.desktop_entries: Iterable[DesktopEntryModel] = []

    def update_desktop_entry(self, content):
        path = content['path']
        de = DesktopEntry.DesktopEntry(path)
        de.set("Name", content['name'])

        if 'categories' in content:
            de.set("Categories", ";".join(content['categories']))

        if 'keywords' in content:
            de.set("Keywords", ";".join(content['keywords']))

        if 'hidden' in content:
            de.set("Hidden", str(content['hidden']).lower())

        try:
            de.write(path)
        except PermissionError:
            pass
        self.update_entries()

    @staticmethod
    def create_desktop_entry(file_name: str, name: str, keywords, icon: str, exec: str, categories):
        base_desktop_entries_path = f'{Path.home()}/.local/share/applications'
        if not os.path.exists(base_desktop_entries_path):
            os.makedirs(base_desktop_entries_path)
        desktop_entries_path = f'{base_desktop_entries_path}/{file_name}.desktop'

        de = DesktopEntry.DesktopEntry()
        de.addGroup('Desktop Entry')
        de.set("Name", name)
        de.set("Terminal", False)
        de.set("Comment", name)
        de.set("Type", "Application")
        de.set("Version", "1.0")
        if categories is not None:
            de.set("Categories", ";".join(categories))
        if keywords is not None:
            de.set("Keywords", ";".join(keywords))
        if icon is not None:
            de.set("Icon", icon)
        if exec is not None:
            de.set("Exec", exec)
        try:
            de.write(desktop_entries_path)
            print('Done write')
        except PermissionError:
            pass

    def update_entries(self):
        self.desktop_entry_cache.clear()
        self.desktop_entries.clear()
        self.category_to_desktop_entries.clear()
        self.filename_to_desktop_entries.clear()
        for app_info in Gio.DesktopAppInfo.get_all():
            desktop_entry = DesktopEntryModel(app_info)
            self.desktop_entries.append(desktop_entry)
            self.desktop_entry_cache[desktop_entry.id] = desktop_entry
            self.filename_to_desktop_entries[desktop_entry.file_name] = desktop_entry
            for category in desktop_entry.categories:
                self.category_to_desktop_entries.setdefault(category, []).append(desktop_entry)

    def get_entry_by_id(self, entry_id) -> DesktopEntryModel:
        if entry_id in self.desktop_entry_cache:
            return self.desktop_entry_cache[entry_id]
        else:
            self.update_entries()
            if entry_id in self.desktop_entry_cache:
                return self.desktop_entry_cache[entry_id]
            else:
                return None


de_repo = DesktopEntryRepository()
de_repo.update_entries()
