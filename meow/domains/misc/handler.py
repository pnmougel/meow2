import os
from signal import SIGTERM

from flask import Blueprint
from flask import send_from_directory
from flask_json import json_response

from common.root.root_client import root_client
from threading import Timer

file_server_handler = Blueprint('file_server_handler', __name__)
kill_handler = Blueprint('kill_handler', __name__)
sudo_switch_handler = Blueprint('sudo_switch_handler', __name__)
is_alive_handler = Blueprint('is_alive_handler', 'is_alive_handler')


@file_server_handler.route('/<path:path>')
def send_js(path):
    return send_from_directory('www', path)


@kill_handler.route('/kill')
def kill_server():
    def self_kill():
        os.kill(os.getpid(), SIGTERM)
    Timer(0.5, self_kill).start()
    return json_response(data_={
        "status": "ok"
    })


@sudo_switch_handler.route('/start_root')
def start_root_server():
    key = root_client.start_root_server()
    return json_response(data_={
        "token": key
    })


@sudo_switch_handler.route('/is_root_up')
def is_root_server_up():
    is_root_up = root_client.is_root_server_up()
    return json_response(data_={
        "is_root_server_alive": is_root_up
    })


@is_alive_handler.route('/is_alive')
def is_alive():
    return json_response(data_={
        "is_alive": True
    })
