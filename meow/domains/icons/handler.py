from flask_json import json_response

from flask import Blueprint, send_file

from domains.desktop_entries.repository import DesktopEntryRepository
from domains.icons.repository import icon_repo

icons_handler = Blueprint('icons_handler', __name__)
de_repository = DesktopEntryRepository()


import logging
logger = logging.getLogger(__name__)


@icons_handler.route('/icon/<entry_id>', methods=['GET'])
def get_desktop_entry_icon(entry_id):
    try:
        elems = entry_id.split('.')
        icon_path = None
        if len(elems) == 2:
            entry = de_repository.get_entry_by_id(elems[0])
            if entry is not None:
                icon_path = entry.icon_path
        if icon_path is not None:
            return send_file(icon_path)
    except Exception as e:
        logger.error('Unable to get icon', e)
        return json_response(data_={'error': 'unable to get icon'})


@icons_handler.route('/icon_by_name/<icon_name>', methods=['GET'])
def get_icon_by_name(icon_name):
    try:
        icon_path = icon_repo.get_icon_by_name(icon_name)
        if icon_path is not None:
            return send_file(icon_path)
        else:
            return json_response(data_={'error': 'unable to get icon'})
    except Exception as e:
        logger.error('Unable to get icon', e)
        return json_response(data_={'error': 'unable to get icon'})


@icons_handler.route('/search_icon/<icon_name>', methods=['GET'])
def search_icon(icon_name):
    icons = icon_repo.search_icon(icon_name)
    return json_response(data_=icons)


