import os

import gi
from gi.repository.Gtk import IconInfo

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio
from gi.repository.Gio import DesktopAppInfo, ThemedIcon, FileIcon


class IconRepository:
    def __init__(self):
        self.icon_theme = Gtk.IconTheme.get_default()
        # for icon in self.icon_theme.list_icons(None):
        #     print(icon)

    def get_icon_path(self, icon):
        if os.path.isfile(icon):
            return icon
        else:
            icon_info = self.icon_theme.lookup_icon(icon, 128, 0)
            if icon_info is not None:
                return icon_info.get_filename()
            else:
                return None

    def get_icon_path(self, icon):
        icon_path = None
        if icon is not None:
            if isinstance(icon, FileIcon):
                icon_path = icon.get_file().get_path()
                if not os.path.isfile(icon_path):
                    icon_path = None
            elif isinstance(icon, ThemedIcon):
                for icon_name in icon.get_names():
                    if icon_path is None:
                        icon_info = self.icon_theme.lookup_icon(icon_name, 128, 0)
                        if icon_info is not None:
                            icon_path = icon_info.get_filename()
        return icon_path

    def get_icon_by_name(self, icon_name):
        icon_info: IconInfo = self.icon_theme.lookup_icon(icon_name, 128, 0)
        if icon_info is not None:
            return icon_info.get_filename()
        else:
            return None

    def search_icon(self, name_to_search):
        matching_icons = []
        name_to_search = name_to_search.lower()
        for icon_name in self.icon_theme.list_icons(None):

            idx = icon_name.lower().find(name_to_search)
            if idx != -1:
                if name_to_search == icon_name:
                    sort_score = 0
                elif idx == 0:
                    sort_score = 1
                else:
                    sort_score = 1 + idx
                matching_icons.append((icon_name, sort_score))

        matching_icons.sort(key=lambda e: e[1])
        return [e[0] for e in matching_icons]


icon_repo = IconRepository()
