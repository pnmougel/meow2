from flask import Blueprint
from flask_json import json_response

# from common.json_encoder import CustomJSONEncoder
from domains.app_folders.repository import folder_repo
from domains.desktop_entries.repository import de_repo

app_folders_handler = Blueprint('app_folders_handler', __name__)

import logging
logger = logging.getLogger(__name__)


@app_folders_handler.route('/app_folders', methods=['GET'])
def get_app_folders():
    de_repo.update_entries()
    app_folders = folder_repo.get_folders()

    de_in_folder = set()
    de_not_in_folder = []
    for app_folder in app_folders:
        de_in_folder = de_in_folder.union([de.id for de in app_folder.get_desktop_entries()])

    for de in de_repo.desktop_entries:
        if de.id not in de_in_folder:
            de_not_in_folder.append(de.to_dict_simple())

    de_not_in_folder.sort(key=lambda x: x['name'].lower())
    # CustomJSONEncoder.json_convert_method = 'to_json_2'
    app_folders = [folder.to_json_with_entries() for folder in app_folders]
    app_folders.append({
        "entries": de_not_in_folder,
        "name": "Not categorized"
    })
    return json_response(data_=list(app_folders))


@app_folders_handler.route('/app_folders/<folder_id>/add_entry/<entry_id>', methods=['POST'])
def add_desktop_entry(folder_id, entry_id):
    folder = folder_repo.get_folder(folder_id)
    if folder is not None:
        folder.add_desktop_entry(entry_id)
    return json_response(data_={'status': 'ok'})


@app_folders_handler.route('/app_folders/<folder_id>/remove_entry/<entry_id>', methods=['POST'])
def remove_desktop_entry(folder_id, entry_id):
    folder = folder_repo.get_folder(folder_id)
    if folder is not None:
        folder.remove_desktop_entry(entry_id)
    return json_response(data_={'status': 'ok'})


@app_folders_handler.route('/app_folders/<folder_id>', methods=['POST'])
def add_desktop_folder(folder_id):
    folder_repo.create_folder(folder_id)
    return json_response(data_={'status': 'ok'})


@app_folders_handler.route('/app_folders/<folder_id>', methods=['DELETE'])
def remove_desktop_folder(folder_id):
    folder_repo.remove_folder(folder_id)
    return json_response(data_={'status': 'ok'})


@app_folders_handler.route('/app_folders/<folder_id>/rename/<new_name>', methods=['POST'])
def rename_desktop_folder(folder_id, new_name):
    folder = folder_repo.get_folder(folder_id)
    if folder is not None:
        folder.set_name(new_name)
    return json_response(data_={'status': 'ok'})

