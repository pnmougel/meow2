import os

from xdg.DesktopEntry import DesktopEntry
import gi

from domains.desktop_entries.model import DesktopEntryModel
from domains.desktop_entries.repository import de_repo

gi.require_version('Gtk', '3.0')
from gi.repository import Gio
from typing import Iterable


# See https://github.com/sysvii/gnome-app-folder/blob/master/gnome-app-folder.py

class AppFolder:
    schema_folder = 'org.gnome.desktop.app-folders.folder'

    def __init__(self, folder_name: str):
        self.id = folder_name
        folder_path = f"/org/gnome/desktop/app-folders/folders/{folder_name}/"
        self.settings: Gio.Settings = Gio.Settings.new_with_path(schema_id=AppFolder.schema_folder, path=folder_path)

        keys = self.settings.list_keys()
        self.name = self.settings.get_string('name') if 'name' in keys else None
        self.categories = self.settings.get_strv('categories') if 'categories' in keys else []
        self.apps = self.settings.get_strv('apps') if 'apps' in keys else []
        self.excluded_apps = self.settings.get_strv('excluded-apps') if 'excluded-apps' in keys else []

        self.icon = None
        desktop_directory_path = f"/usr/share/desktop-directories/{self.name}"
        if os.path.isfile(desktop_directory_path):
            directory_entry = DesktopEntry(desktop_directory_path)
            self.displayed_name = directory_entry.getName()
            if directory_entry.getIcon() is not '':
                self.icon = directory_entry.getIcon()
        else:
            self.displayed_name = self.name

    def add_desktop_entry(self, desktop_entry_id: str):
        desktop_entry = de_repo.get_entry_by_id(desktop_entry_id)
        if desktop_entry is not None and desktop_entry.file_name is not None:
            if desktop_entry.file_name not in self.apps:
                self.apps.append(desktop_entry.file_name)
                self.settings.set_strv('apps', self.apps)

            if desktop_entry.file_name in self.excluded_apps:
                self.excluded_apps.remove(desktop_entry.file_name)
                self.settings.set_strv('excluded-apps', self.excluded_apps)

    def remove_desktop_entry(self, desktop_entry_id: str):
        desktop_entry = de_repo.get_entry_by_id(desktop_entry_id)
        if desktop_entry is not None and desktop_entry.file_name is not None:
            if desktop_entry.file_name in self.apps:
                self.apps.remove(desktop_entry.file_name)
                self.settings.set_strv('apps', self.apps)

            if desktop_entry.file_name not in self.excluded_apps:
                self.excluded_apps.append(desktop_entry.file_name)
                self.settings.set_strv('excluded-apps', self.excluded_apps)

    def get_desktop_entries(self) -> Iterable[DesktopEntryModel]:
        desktop_entries = []
        excluded_ids = set()
        for excluded_apps in self.excluded_apps:
            if excluded_apps in de_repo.filename_to_desktop_entries:
                excluded_ids.add(de_repo.filename_to_desktop_entries[excluded_apps].id)

        for app_name in self.apps:
            de = de_repo.filename_to_desktop_entries.get(app_name, None)
            if de is not None and de.id not in excluded_ids:
                excluded_ids.add(de.id)         # Add to excluded ids to avoid duplicate entries
                desktop_entries.append(de)

        for category in self.categories:
            for de in de_repo.category_to_desktop_entries.get(category, []):
                if de.id not in excluded_ids:
                    excluded_ids.add(de.id)     # Add to excluded ids to avoid duplicate entries
                    desktop_entries.append(de)

        def sort_by_name(e: DesktopEntryModel):
            return e.name.lower()
        desktop_entries.sort(key=sort_by_name)
        return desktop_entries

    def set_name(self, new_name: str):
        if new_name is not None and isinstance(new_name, str):
            self.settings.set_string('name', new_name)
            self.name = new_name

    def to_json(self):
        return {
            'id': self.id,
            'name': self.displayed_name
        }

    def to_json_with_entries(self):
        return {
            'id': self.id,
            'name': self.displayed_name,
            'entries': [de.to_dict_simple() for de in self.get_desktop_entries()]
        }

