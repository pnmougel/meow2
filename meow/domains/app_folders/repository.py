
import gi

from domains.app_folders.model import AppFolder
from typing import Mapping

gi.require_version('Gtk', '3.0')
from gi.repository import Gio


class AppFolderRepository:
    def __init__(self):
        self.folders: Mapping[str, AppFolder] = {}
        self.app_folder_key = 'folder-children'
        self.schema_app_folder = 'org.gnome.desktop.app-folders'
        self.schema_folder = 'org.gnome.desktop.app-folders.folder'
        # if self.schema_app_folder in Gio.Settings.list_schemas():
        self.settings = Gio.Settings(self.schema_app_folder)

    def get_folders(self):
        self.update_folders()
        return self.folders.values()

    def update_folders(self):
        self.folders.clear()
        if self.app_folder_key in self.settings.list_keys():
            for folder_name in self.settings.get_strv(self.app_folder_key):
                app_folder = AppFolder(folder_name)
                self.folders[app_folder.id] = app_folder

    def get_folder(self, folder_id):
        return self.folders.get(folder_id)

    def remove_folder(self, folder_id):
        self.update_folders()
        if folder_id in self.folders:
            app_folder = self.folders[folder_id]
            for key in app_folder.settings.list_keys():
                app_folder.settings.reset(key)

            app_folders = self.settings.get_strv(self.app_folder_key)
            app_folders.remove(folder_id)
            self.settings.set_strv('folder-children', app_folders)

    def create_folder(self, folder_name):
        app_folders = self.settings.get_strv(self.app_folder_key)
        name = folder_name

        # Generate unique name for the app folder
        i = 1
        while folder_name in app_folders:
            folder_name = f'{name}_{i}'
            i += 1

        app_folders.append(folder_name)
        self.settings.set_strv('folder-children', app_folders)

        self.update_folders()
        new_folder = self.get_folder(folder_name)
        new_folder.set_name(name)
        return new_folder


folder_repo = AppFolderRepository()
folder_repo.update_folders()
