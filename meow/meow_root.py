#!/usr/bin/env python3.6


import os
import sys

from flask_cors import CORS
from flask_json import FlaskJSON
from gevent.pywsgi import WSGIServer

from domains.desktop_entries.handler import desktop_entry_update_handler

cur_dir = os.path.dirname(__file__)
if cur_dir not in sys.path:
    sys.path.append(os.path.dirname(__file__))

from common.json_encoder import CustomJSONEncoder
from common.logs import init_logs

from domains.misc.handler import kill_handler, is_alive_handler
from flask import Flask, request, Response
from time import sleep

import logging
import random

AUTHORIZED_PATHS = ['/kill', '/is_alive']
SERVER_PORT = 20101
logger = logging.getLogger(__name__)


def get_server(key, is_prod = False):
    init_logs(is_prod)

    app = Flask(__name__)
    FlaskJSON(app)
    CORS(app)
    app.json_encoder = CustomJSONEncoder

    app.register_blueprint(desktop_entry_update_handler)
    app.register_blueprint(kill_handler)
    app.register_blueprint(is_alive_handler)

    app.config.update({"JSON_ADD_STATUS": False})

    @app.before_request
    def check_authentication():
        auth = request.headers.get('Authorization')
        if auth != key and request.path not in AUTHORIZED_PATHS and request.method != 'OPTIONS':
            # Avoid attacks based on the time it takes to compute equality
            # Since the application run in local environment such attacks can be used
            sleep(random.random())
            return Response(
                'Could not verify your access level for that URL.\n'
                'You have to login with proper credentials', 401,
                {'WWW-Authenticate': 'Basic realm="Login Required"'})
    return app


def read_line():
    line = ''
    c = ''
    while c != '\n':
        c = sys.stdin.read(1)
        if c != '\n':
            line += c
    return line


def start_root_server():
    key = read_line()
    app = get_server(key, True)

    http_server = WSGIServer(('', SERVER_PORT), app)
    try:
        print('Starting server')
        http_server.serve_forever()
    except Exception as e:
        logger.error("Unable to start root server", e)


if __name__ == '__main__':
    start_root_server()
