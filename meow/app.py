#! /usr/bin/env python3.6

import sys
import os

cur_dir = os.path.dirname(__file__)
if cur_dir not in sys.path:
    sys.path.append(os.path.dirname(__file__))

from front import start_gui
from meow_server import start_prod_server

import threading


def run_all(is_prod=True):
    threading.Thread(target=start_gui).start()
    start_prod_server()


if __name__ == "__main__":
    run_all(False)
